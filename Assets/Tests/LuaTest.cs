﻿using System;
using NLua;
using NUnit.Framework;

public class LuaTest
{
    [Test]
    public void TestBasicLogic()
    {
        var lua = new LuaWrapper();
        lua.SetBool("a_happy", true);
        lua.SetBool("b_happy", false);
        Assert.IsTrue(lua.Condition("a_happy or b_happy"));
        Assert.IsFalse(lua.Condition("a_happy and b_happy"));
    }

    [Test]
    public void TestBasicArithmetics()
    {
        var lua = new LuaWrapper();
        lua.SetInt("a_money", 5);
        lua.Eval("a_money = a_money + 5");
        Assert.AreEqual(lua.GetInt("a_money"), 10);

        lua.SetFloat("a_power", 0.1f);
        lua.Eval("a_power = 10 * a_money * a_power + 100");
        Assert.AreEqual(lua.GetFloat("a_power"), 110f);
    }

    [Test]
    public void TestBasicString()
    {
        var lua = new LuaWrapper();
        lua.SetString("a_name", "Fucker");
        Assert.AreEqual(lua.GetString("a_name"), "Fucker");
        Assert.IsTrue(lua.Condition("a_name == \"Fucker\""));
    }
}
