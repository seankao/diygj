﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLua;
using NUnit.Framework;
using Operator.Data;

public class GameplayTest
{
    [Test]
    public void TestOptionEvals()
    {
        var system = _LoadSystem();
        var options = TextAssetLoader.Load<List<Option>>("DataSheets/options");
        foreach (Option option in options)
            system.Eval(option.Eval);
    }

    [Test]
    public void TestPhonecallConditions()
    {
        var system = _LoadSystem();
        var phonecalls = TextAssetLoader.Load<List<PhoneCall>>("DataSheets/phonecalls");
        foreach (PhoneCall phonecall in phonecalls)
            system.Condition(phonecall.Condition);
    }

    [Test]
    public void TestOptionConditions()
    {
        var system = _LoadSystem();
        var options = TextAssetLoader.Load<List<Option>>("DataSheets/options");
        foreach (Option option in options)
            system.Condition(option.Condition);
    }

    [Test]
    public void TestEndingConditions()
    {
        var system = _LoadSystem();
        var endings = TextAssetLoader.Load<List<Ending>>("DataSheets/endings");
        foreach (Ending ending in endings)
            system.Condition(ending.Condition);
    }

    private static LuaWrapper _LoadSystem()
    {
        var variables = TextAssetLoader.Load<List<Variable>>("DataSheets/variables");
        var system = new LuaWrapper(variables);
        system.Add("is_end", false);
        system.Add("round", 0);
        system.Add("pickup_phonecalls", 0);
        return system;
    }
}
