﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Operator.Tutorial
{
    public class TutorialView : MonoBehaviour
    {
        [SerializeField]
        private Text _text;
        [SerializeField]
        private Button _skipButton;
        [SerializeField]
        private Button _startButton;
        [SerializeField]
        private CanvasGroup _darkness;

        public event Action OnStart;
        public event Action OnSkip;

        private const float FADE_DURATION = 1.3f;

        private void Awake()
        {
            _startButton.onClick.AddListener(() => OnStart?.Invoke());
            _skipButton.onClick.AddListener(() => OnSkip?.Invoke());
        }

        public void Init()
        {
            _darkness.alpha = 1f;
            SetInteractive(false);
        }

        public void SetInteractive(bool interactive)
        {
            _darkness.blocksRaycasts = !interactive;
            _darkness.interactable = !interactive;
        }

        public void SetText(string text)
            => _text.text = text;

        public UniTask FadeIn(CancellationToken ct)
            => _darkness.FadeOut(FADE_DURATION)
                .ToUniTask(cancellationToken: ct);
    }
}

