﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace Operator.Tutorial
{
    public class TutorialScene : MonoBehaviour
    {
        [SerializeField]
        private TutorialView _view;

        private string _text =
@"經歷一個瘋狂的夜晚之後，你醒來、發現自己處於一個陌生的建築前。建築內的人走了出來，他們告訴你這裡是「超時空電信局」，而你顯然是意外被傳送過來了。他們請你乾脆為他們工作、擔任他們的接線生，或許他們能找到送你回去的方法。

你想了想，也別無選擇。你來到了一塊奇妙的面板前坐下，開始嘗試為不同時空的人們接通來電...

操作：
單擊來電選取、再按一下取消。選取時點擊下方接聽者即可接通來電。
滑鼠移動到上方或下方，滾輪可以滾動列表。
右下方「通話記錄」可以查看接通後發生的事。";


        private CancellationTokenSource _skip = new CancellationTokenSource();

        private void Awake()
        {
            Init();
        }

        public UniTask Run(CancellationToken ct)
            => _Run(ct);

        private bool _finished = false;

        public void Init()
        {
            _view.Init();
        }

        private async UniTask _Run(CancellationToken ct)
        {
            _view.Init();
            await _view.FadeIn(ct);
            _Bind(_view);
            _view.SetInteractive(true);
            
            _DisplayDescription(_text, (_skip = new CancellationTokenSource()).Token);
            await UniTask.WaitUntil(() => _finished, cancellationToken: ct);
            _skip.Cancel();

            _view.SetInteractive(false);
            _Unbind(_view);
        }

        private void _Bind(TutorialView view)
        {
            _view.OnStart += _Leave;
            _view.OnSkip += _Skip;
        }

        private void _Unbind(TutorialView view)
        {
            _view.OnStart -= _Leave;
            _view.OnSkip -= _Skip;
        }

        private void _Leave()
        {
            _finished = true;
        }

        private void _Skip()
        {
            _skip.Cancel();
        }

        private async UniTask _DisplayDescription(string description, CancellationToken ct)
        {
            try
            {
                await Easing.Play(0, description.Length, description.Length * 0.04f, Easing.Interpolators.Linear, fLength =>
                {
                    _view.SetText(description.Substring(0, Mathf.CeilToInt(fLength)));
                })
                .ToUniTask(cancellationToken: ct);
            }
            catch (OperationCanceledException e)
            { }
            finally
            {
                _view.SetText(description);
            }
        }
    }
}

