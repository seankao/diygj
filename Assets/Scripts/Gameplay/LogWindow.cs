﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LogWindow : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _panel;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Text _logView;

    [SerializeField]
    private AudioSource openSound;
    
    [SerializeField]
    private AudioSource closeSound;


    public event Action OnClose;

    private void Awake()
    {
        _closeButton.onClick.AddListener(() => OnClose?.Invoke());
    }

    public void SetLog(string log)
        => _logView.text = log;

    public void Open()
    {
        openSound.Play();
        _panel.interactable = true;
        _panel.blocksRaycasts = true;
        _panel.alpha = 1f;
    }

    public void Close()
    {
        closeSound.Play();
        _panel.interactable = false;
        _panel.blocksRaycasts = false;
        _panel.alpha = 0f;
    }
}
