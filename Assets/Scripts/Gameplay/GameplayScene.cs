﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

namespace Operator.Gameplay
{
    using Data;

    public class ConnectData
    {
        public string CallerPhoneId;
        public string Receiver;
    }

    public class GameplayScene : MonoBehaviour
    {
        [SerializeField]
        PhoneCallManager _phoneCallManager;
        [SerializeField]
        Text _infoView;
        [SerializeField]
        LogWindow _logWindow;
        [SerializeField]
        Button _openLogWindowButton;

        LuaWrapper _system;
        List<Person> _people;
        List<PhoneCall> _phonecalls;
        List<Option> _options;
        Ending[] _endings;
        Dictionary<string, PhoneCall> _phonecallByIds;
        OptionMatcher _optionMatcher;
        List<string> _activePhonecalls;
        HashSet<string> _dialedPhonecalls;
        string _log = string.Empty;

        private PhoneCall _hoveringCaller;
        private Caller _pendingCaller;

        const string VAR_IS_END = "is_end";
        const string VAR_ROUND = "round";
        const string VAR_PICKUP_PHONECALLS = "pickup_phonecalls";

        UniTaskCompletionSource<ConnectData> _connectInput = new UniTaskCompletionSource<ConnectData>();

        private void Awake()
        {
            _activePhonecalls = new List<string>();
            _dialedPhonecalls = new HashSet<string>();

            _logWindow.OnClose += _logWindow.Close;
            _openLogWindowButton.onClick.AddListener(() =>
            {
                _logWindow.SetLog(_log);
                _logWindow.Open();
            });

            _phoneCallManager.event_Connect.AddListener((callerData, receiverData) =>
                _connectInput.TrySetResult(new ConnectData
                {
                    CallerPhoneId = callerData.Id,
                    Receiver = receiverData.Name
                })
            );

            _phoneCallManager.OnChangeHoveringCaller += hoveringCaller =>
            {
                _UpdateInfoText(_hoveringCaller = hoveringCaller, _pendingCaller);
            };

            _phoneCallManager.event_PendingCallerChange.AddListener(pendingCaller =>
            {
                _UpdateInfoText(_hoveringCaller, _pendingCaller = pendingCaller);
            });
        }

        private void _UpdateInfoText(PhoneCall hoverCaller, Caller pendingCaller) 
        {
            if (hoverCaller != null && _phonecallByIds.ContainsKey(hoverCaller.Id))
                _infoView.text = _phonecallByIds[hoverCaller.Id].Info;
            else if (pendingCaller != null && _phonecallByIds.ContainsKey(pendingCaller.Data.Id))
                _infoView.text = _phonecallByIds[pendingCaller.Data.Id].Info;
            else
                _infoView.text = string.Empty;
        }

        public async UniTask<GameplayResult> Run(CancellationToken ct)
        {
            await _Init(ct);
            Ending ending = await _MainLoop(ct);
            return new GameplayResult { EndingId = ending.Id };
        }

        private async UniTask _Init(CancellationToken ct)
        {
            var variables = await TextAssetLoader.LoadAsync<List<Variable>>("DataSheets/variables", ct);
            _people = await TextAssetLoader.LoadAsync<List<Person>>("DataSheets/people", ct);
            _phonecalls = await TextAssetLoader.LoadAsync<List<PhoneCall>>("DataSheets/phonecalls", ct);
            _options = await TextAssetLoader.LoadAsync<List<Option>>("DataSheets/options", ct);
            _endings = await TextAssetLoader.LoadAsync<Ending[]>("DataSheets/endings", ct);

            _phonecallByIds = _phonecalls.ToDictionary(phonecall => phonecall.Id, call => call);
            _optionMatcher = new OptionMatcher(_options);

            foreach (var person in _people)
                _phoneCallManager.AddReceiver(person);

            _system = new LuaWrapper(variables);
            _system.Add(VAR_IS_END, false);
            _system.Add(VAR_ROUND, 0);
            _system.Add(VAR_PICKUP_PHONECALLS, 0);
        }
        
        private async UniTask<Ending> _MainLoop(CancellationToken ct)
        {
            while (true)
            {
                var incomingPhonecalls = _phonecalls.Where(phonecall =>
                    !_dialedPhonecalls.Contains(phonecall.Id) && _system.Condition(phonecall.Condition));
                if (incomingPhonecalls.Count() == 0 && _activePhonecalls.Count == 0)
                    break;

                foreach (var phonecall in incomingPhonecalls)
                {
                    _activePhonecalls.Add(phonecall.Id);
                    _phoneCallManager.AddCaller(phonecall);
                    _dialedPhonecalls.Add(phonecall.Id);
                }

                _system.SetInt(VAR_ROUND, _system.GetInt(VAR_ROUND) + 1);

                var connect = await _WaitForConnected();
                _activePhonecalls.Remove(connect.CallerPhoneId);
                _phoneCallManager.RemoveCaller(connect.CallerPhoneId);
                _system.Add(connect.CallerPhoneId, connect.Receiver);
                _system.SetInt(VAR_PICKUP_PHONECALLS, _system.GetInt(VAR_PICKUP_PHONECALLS) + 1);
                foreach(var option in _optionMatcher.Match(connect.CallerPhoneId, connect.Receiver)
                    .Where(option => _system.Condition(option.Condition)))
                {
                    _system.Eval(option.Eval);
                    if(!string.IsNullOrEmpty(option.Receiver))
                        _log += $"<size=34>將<color=#61bd4f>{_phonecallByIds[connect.CallerPhoneId].Caller}</color>轉接給<color=#4766e2>{connect.Receiver}</color></size>{Environment.NewLine}{Environment.NewLine}";
                    _log += $"<size=36>{option.Result}</size>{Environment.NewLine}{Environment.NewLine}";
                }

                if (_IsEnded(_system, _endings, out Ending ending))
                    return ending;
            }

            _system.SetBool(VAR_IS_END, true);

            {
                if (_IsEnded(_system, _endings, out Ending ending))
                    return ending;
                else
                    return null;
            }
        }

        private async UniTask<ConnectData> _WaitForConnected()
        {
            _connectInput = new UniTaskCompletionSource<ConnectData>();
            return await _connectInput.Task;
        }
        
        private void _ShowEnding(LuaWrapper system, Ending[] endings)
        {
            foreach (var ending in endings)
            {
                if (system.Condition(ending.Condition))
                {
                    //_messageView.text += $"Enter {ending.Id}: {ending.Condition}{Environment.NewLine}";
                    break;
                }
            }
        }

        private bool _IsEnded(LuaWrapper system, Ending[] endings, out Ending ending)
        {
            ending = null;
            foreach (var e in endings)
            {
                if (system.Condition(e.Condition))
                {
                    ending = e;
                    return true;
                }
            }
            return false;
        }
    }
}

