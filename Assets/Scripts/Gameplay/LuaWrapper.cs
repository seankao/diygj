﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using NLua;
using System;

public class LuaWrapper
{
    private Lua _lua;
    private List<Variable> _variables;

    public IEnumerable<Variable> Variables
        => _variables;

    public event Action OnUpdate;

    public LuaWrapper(IEnumerable<Variable> vs) : this()
    {
        foreach (var v in vs)
            Add(v);
    }

    public LuaWrapper()
    {
        _lua = new Lua();
        _variables = new List<Variable>();
    }

    public void Add(Variable v)
    {
        _Add(v);
        OnUpdate?.Invoke();
    }

    private void _Add(Variable v)
    {
        switch (v.Type)
        {
            case "int":
                _lua[v.Name] = 0;
                break;
            case "float":
                _lua[v.Name] = 0f;
                break;
            case "bool":
                _lua[v.Name] = false;
                break;
            case "string":
                _lua[v.Name] = string.Empty;
                break;
        }
        _variables.Add(v);
    }


    public void Add(string name, string s)
    {
        _Add(new Variable
        {
            Name = name,
            Type = "string"
        });
        SetString(name, s);
    }

    public void Add(string name, int i)
    {
        _Add(new Variable
        {
            Name = name,
            Type = "int"
        });
        SetInt(name, i);
    }

    public void Add(string name, float f)
    {
        _Add(new Variable
        {
            Name = name,
            Type = "float"
        });
        SetFloat(name, f);
    }

    public void Add(string name, bool b)
    {
        _Add(new Variable
        {
            Name = name,
            Type = "bool"
        });
        SetBool(name, b);
    }

    public int GetInt(string name)
        => Convert.ToInt32(_lua[name]);

    public void SetInt(string name, int value)
    {
        _lua[name] = value;
        OnUpdate?.Invoke();
    }

    public float GetFloat(string name)
        => Convert.ToSingle(_lua[name]);

    public void SetFloat(string name, float value)
    {
        _lua[name] = value;
        OnUpdate?.Invoke();
    }
    
    public bool GetBool(string name)
        => (bool)_lua[name];

    public void SetBool(string name, bool value)
    {
        _lua[name] = value;
        OnUpdate?.Invoke();
    }

    public string GetString(string name)
        => (string)_lua[name];

    public void SetString(string name, string value)
    {
        _lua[name] = value;
        OnUpdate?.Invoke();
    }

    public void Eval(string expression)
    {
        try
        {
            _lua.DoString(expression);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            throw new Exception("Error executing command: " + expression);
        }
        finally
        {
            OnUpdate?.Invoke();
        }
    }

    public bool Condition(string condition)
    {
        try
        {
            return (bool)_lua.DoString("return " + condition)[0];
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            throw new Exception("Error executing condition: " + condition);
        }
    }
}