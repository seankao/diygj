﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class CallerFactory : ScriptableObject
{
    public static Sprite GetAvatar(string avatarId)
    {
        return Resources.Load<Sprite>($"Avatar/{avatarId}");
    }
}