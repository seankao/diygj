﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OptionMatcher
{
    private Dictionary<string, OptionReceiverMatcher> _recieverMatchers;

    public OptionMatcher(IEnumerable<Option> options)
    {
        _recieverMatchers = options
            .GroupBy(option => option.PhoneCallId)
            .ToDictionary(group => group.Key, group => new OptionReceiverMatcher(group));
    }

    public IEnumerable<Option> Match(string phonecallId, string receiver)
    {
        if (!_recieverMatchers.ContainsKey(phonecallId))
            return Enumerable.Empty<Option>();
        return _recieverMatchers[phonecallId].Match(receiver);
    }

}


public class OptionReceiverMatcher
{
    private List<Option> _options;
    private Option _other;
    private Option _always;

    public OptionReceiverMatcher(IEnumerable<Option> options)
    {
        _options = options.ToList();
        _other = _options.Find(option => option.Receiver == "other");
        _always = _options.Find(option => string.IsNullOrEmpty(option.Receiver));
    }

    public IEnumerable<Option> Match(string reciever)
    {
        var options = _options.Where(option => option.Receiver == reciever).ToList();
        if (_other != null && options.Count == 0)
            options.Add(_other);
        if (_always != null)
            options.Add(_always);
        return options;
    }
}