﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Operator.Menu
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup _panel;
        [SerializeField]
        private Button _startButton;
        [SerializeField]
        private Button _endButton;
        [SerializeField]
        private CanvasGroup _darkness;

        public event Action OnStart;
        public event Action OnEnd;

        private const float FADE_DURATION = 1.3f;

        private void Awake()
        {
            _startButton.onClick.AddListener(() => OnStart?.Invoke());
            _endButton.onClick.AddListener(() => OnEnd?.Invoke());
        }

        public void Init()
        {
            _darkness.alpha = 1f;
            _darkness.blocksRaycasts = true;
        }

        public void SetInteractive(bool interactive)
        {
            _darkness.blocksRaycasts = !interactive;
        }

        public UniTask FadeIn(CancellationToken ct)
            => _darkness.FadeOut(FADE_DURATION)
                .ToUniTask(cancellationToken: ct);
    }
}

