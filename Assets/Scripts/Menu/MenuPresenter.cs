﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Operator.Menu
{
    public class MenuPresenter
    {
        private MenuView _view;
        private bool _finished = false;

        public MenuPresenter(MenuView view)
        {
            _view = view;
        }

        public void Init()
            => _view.Init();

        public async UniTask Run(CancellationToken ct)
        {
            _view.Init();
            await _view.FadeIn(ct);
            _Bind(_view);
            _view.SetInteractive(true);
            await UniTask.WaitUntil(() => _finished, cancellationToken: ct);
            _view.SetInteractive(false);
            _Unbind(_view);
        }

        private void _Bind(MenuView view)
        {
            _view.OnStart += _Leave;
            _view.OnEnd += _CloseGame;
        }

        private void _Unbind(MenuView view)
        {
            _view.OnStart -= _Leave;
            _view.OnEnd -= _CloseGame;
        }

        private void _Leave()
        {
            _finished = true;
        }

        private void _CloseGame()
        {
            Application.Quit();
        }
    }
}

