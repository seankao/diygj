﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Operator.Menu
{
    public class MenuScene : MonoBehaviour
    {
        [SerializeField]
        private MenuView _view;

        private MenuPresenter _presenter;

        private void Awake()
        {
            _presenter = new MenuPresenter(_view);
            _presenter.Init();
        }

        public UniTask Run(CancellationToken ct)
            => _presenter.Run(ct);
    }
}

