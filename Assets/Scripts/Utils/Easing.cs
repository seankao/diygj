﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;

namespace Operator
{
    using Interpolator = Func<float, float>;

    public static class Easing
    {
        public enum Interpolators
        {
            Linear,
            Accel,
            Decel
        }

        public static IEnumerator Play(float src, float dst, float duration, Interpolators interpolator, Action<float> onUpdate)
        {
            float time = 0f;
            while (time < duration)
            {
                float t = Mathf.Lerp(src, dst, _Interpolator(interpolator)(time / duration));
                onUpdate(t);
                time += Time.deltaTime;
                yield return null;
            }
            onUpdate(dst);
        }

        private static Interpolator _Interpolator(Interpolators interpolator)
        {
            switch (interpolator)
            {
                case Interpolators.Accel:
                    return Interpolation.Accel(1f);
                case Interpolators.Decel:
                    return Interpolation.Decel(1f);
                case Interpolators.Linear:
                default:
                    return Interpolation.Linear;
            }

        }
    }

    public static class EasingExtension
    {
        public static IEnumerator FadeIn(this CanvasGroup panel, float duration)
            => Easing.Play(0f, 1f, duration, Easing.Interpolators.Linear,
                alpha => panel.alpha = alpha);

        public static IEnumerator FadeOut(this CanvasGroup panel, float duration)
            => Easing.Play(1f, 0f, duration, Easing.Interpolators.Linear,
                alpha => panel.alpha = alpha);

        public static IEnumerator FadeIn(this Text text, float duration)
            => Easing.Play(0f, 1f, duration, Easing.Interpolators.Linear,
                alpha => {
                    Color c = text.color;
                    c.a = alpha;
                    text.color = c;
                });

        public static IEnumerator FadeOut(this Text text, float duration)
            => Easing.Play(1f, 0f, duration, Easing.Interpolators.Linear,
                alpha => {
                    Color c = text.color;
                    c.a = alpha;
                    text.color = c;
                });

        public static IEnumerator FadeIn(this Image image, float duration)
            => Easing.Play(0f, 1f, duration, Easing.Interpolators.Linear,
                alpha => {
                    Color c = image.color;
                    c.a = alpha;
                    image.color = c;
                });

        public static IEnumerator FadeOut(this Image image, float duration)
            => Easing.Play(1f, 0f, duration, Easing.Interpolators.Linear,
                alpha => {
                    Color c = image.color;
                    c.a = alpha;
                    image.color = c;
                });
    }
}
