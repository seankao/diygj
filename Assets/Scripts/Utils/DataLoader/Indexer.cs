﻿using System.Collections;
using System.Collections.Generic;

public class Indexer
{
    Dictionary<string, int> _indices;

    public Indexer(string[] entries)
    {
        _indices = new Dictionary<string, int>();
        for (int i = 0; i < entries.Length; i++)
            _indices[entries[i]] = i;
    }

    public int Find(string entry)
        => _indices.ContainsKey(entry) ? _indices[entry] : -1;
}