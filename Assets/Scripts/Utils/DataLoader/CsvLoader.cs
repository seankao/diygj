﻿#if UNITY_EDITOR

using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

public static class CsvLoader
{
    public static bool LoadFromFile(string description, out string[][] table)
    {
        table = null;
        string path = EditorUtility.OpenFilePanel(description, "", "");
        if (string.IsNullOrEmpty(path) || !File.Exists(path))
            return false;
        table = File.ReadAllLines(path)
            .Select(line => line
                .Split(',')
                .Select(str => str.Trim())
                .ToArray()
            )
            .ToArray();
        return true;
    }

    public static string[][] parseCSV(string rawData)
    {
        return rawData
            .Split(
                new[] { "\r\n" },
                StringSplitOptions.None
            )
            .Select(line => line.Split(',')
                .Select(str => str.Trim()
            )
            .ToArray()
        )
        .ToArray();
    }
}
#endif