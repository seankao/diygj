﻿#if UNITY_EDITOR

using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.Networking;
using UnityEngine;
using Operator.Data;

[ExecuteInEditMode]
public class SpreadSheetDownloader : EditorWindow
{
    private Queue<IEnumerator> _downloadCoroutines = new Queue<IEnumerator>();

    #region Constants
    private const string DOCUMENT_ID = "14maoWFVJyMv6YifxpyyhNwXA7s4Z4bFgnOJmfTUzhfs";
    private const string VARIABLE_SHEET_ID = "0";
    private const string PEOPLE_SHEET_ID = "1574441076";
    private const string PHONECALL_SHEET_ID = "1855248849";
    private const string OPTION_SHEET_ID = "1687197062";
    private const string ENDING_SHEET_ID = "962994711";

    private const string VARIABLE_DATA_PATH = "Resources/DataSheets/variables.txt";
    private const string PEOPLE_DATA_PATH = "Resources/DataSheets/people.txt";
    private const string PHONECALL_DATA_PATH = "Resources/DataSheets/phonecalls.txt";
    private const string OPTION_DATA_PATH = "Resources/DataSheets/options.txt";
    private const string ENDING_DATA_PATH = "Resources/DataSheets/endings.txt";
    #endregion

    #region Behaviours
    [MenuItem ("Window/Spread Sheet Downloader")]
	public static void ShowWindow() {
		EditorWindow.GetWindow (typeof(SpreadSheetDownloader));
    }

    private string _GetSpreadSheetUrl(string docId, string sheetId)
    {
        return $"https://docs.google.com/spreadsheets/d/" + docId + "/export?format=csv&gid=" + sheetId;
    }

    private bool _IsDownloading
        => _downloadCoroutines.Count > 0;

    private void OnInspectorUpdate()
    {
        if (!_IsDownloading)
            return;
        var coroutine = _downloadCoroutines.Peek();
        bool hasNext = coroutine.MoveNext();
        if (!hasNext)
            _downloadCoroutines.Dequeue();
    }

    void OnGUI()
    {
        EditorGUI.BeginDisabledGroup(_IsDownloading);
        if (GUILayout.Button("Load Variables"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, VARIABLE_SHEET_ID, _LoadVariables);
        }
        if (GUILayout.Button("Load People"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, PEOPLE_SHEET_ID, _LoadPeople);
        }
        if (GUILayout.Button("Load PhoneCalls"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, PHONECALL_SHEET_ID, _LoadPhoneCalls);
        }
        if (GUILayout.Button("Load Options"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, OPTION_SHEET_ID, _LoadOptions);
        }
        if (GUILayout.Button("Load Endings"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, ENDING_SHEET_ID, _LoadEndings);
        }
        if (GUILayout.Button("Load All"))
        {
            _DownloadSpreadSheet(DOCUMENT_ID, VARIABLE_SHEET_ID, _LoadVariables);
            _DownloadSpreadSheet(DOCUMENT_ID, PEOPLE_SHEET_ID, _LoadPeople);
            _DownloadSpreadSheet(DOCUMENT_ID, PHONECALL_SHEET_ID, _LoadPhoneCalls);
            _DownloadSpreadSheet(DOCUMENT_ID, OPTION_SHEET_ID, _LoadOptions);
            _DownloadSpreadSheet(DOCUMENT_ID, ENDING_SHEET_ID, _LoadEndings);
        }
        EditorGUI.EndDisabledGroup();
    }
    #endregion

    #region Functions
    void _DownloadSpreadSheet(string documentId, string sheetId, Action<string[][]> onComplete)
    {
        _downloadCoroutines.Enqueue(new Operator.Coroutine(_DownloadSpreadSheet(documentId, sheetId, data => {
            var table = CsvLoader.parseCSV(data);
            onComplete(table);
        })).Run());
    }

    private IEnumerator _DownloadSpreadSheet(string docId, string gid, Action<string> onComplete)
    {
        string downloadData = null;
        UnityWebRequest request = UnityWebRequest.Get(_GetSpreadSheetUrl(docId, gid));
        yield return _ProcessWebRequest(request.SendWebRequest());
        if (request.isNetworkError)
        {
            Debug.Log($"Network error: {request.error}");
        }
        else
        {
            Debug.Log("Download success");
            downloadData = request.downloadHandler.text;
        }
        onComplete(downloadData);
    }

    private IEnumerator _ProcessWebRequest(UnityWebRequestAsyncOperation op)
    {
        while (!op.isDone)
            yield return null;
    }

    private void _LoadVariables(string[][] table)
    {
        var indexer = new Indexer(table.First());

        List<Variable> variables = new List<Variable>();
        foreach (string[] row in table.Skip(1))
        {
            var variable = new Variable
            {
                Name = row[indexer.Find("Name")],
                Type = row[indexer.Find("Type")],
            };
            variables.Add(variable);
        }
        string path = Path.Combine(Application.dataPath, VARIABLE_DATA_PATH);
        JsonUtils.WriteFile(path, variables);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private void _LoadPhoneCalls(string[][] table)
    {
        var indexer = new Indexer(table.First());

        List<PhoneCall> phonecalls = new List<PhoneCall>();
        foreach (string[] row in table.Skip(1))
        {
            var phonecall = new PhoneCall
            {
                Id = row[indexer.Find("PhoneCall")],
                Caller = row[indexer.Find("Caller")],
                AvatarId = row[indexer.Find("Avatar")],
                Content = _RemoveLineWrap(_Unescape(row[indexer.Find("Content")])),
                Condition = _Unescape(row[indexer.Find("Condition")]),
                Info = _RemoveLineWrap(_Unescape(row[indexer.Find("Info")])),
                Frame = int.Parse(row[indexer.Find("Frame")]),
            };
            if (string.IsNullOrEmpty(phonecall.Condition))
                phonecall.Condition = "true";
            phonecalls.Add(phonecall);
        }
        string path = Path.Combine(Application.dataPath, PHONECALL_DATA_PATH);
        JsonUtils.WriteFile(path, phonecalls);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    private void _LoadOptions(string[][] table)
    {
        var indexer = new Indexer(table.First());

        List<Option> options = new List<Option>();
        foreach (string[] row in table.Skip(1))
        {
            var option = new Option
            {
                Id = row[indexer.Find("Option")],
                PhoneCallId = row[indexer.Find("PhoneCall")],
                Receiver = row[indexer.Find("Receiver")],
                Eval = _Unescape(row[indexer.Find("Eval")]),
                Condition = _Unescape(row[indexer.Find("Condition")]),
                Result = _RemoveLineWrap(_Unescape(row[indexer.Find("Result")])),
            };
            if (string.IsNullOrEmpty(option.Condition))
                option.Condition = "true";
            options.Add(option);
        }
        string path = Path.Combine(Application.dataPath, OPTION_DATA_PATH);
        JsonUtils.WriteFile(path, options);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    private void _LoadEndings(string[][] table)
    {
        var indexer = new Indexer(table.First());

        List<Ending> phonecalls = new List<Ending>();
        foreach (string[] row in table.Skip(1))
        {
            var ending = new Ending
            {
                Id = row[indexer.Find("Ending")],
                Title = row[indexer.Find("Title")],
                Condition = _Unescape(row[indexer.Find("Condition")]),
                Description = _Unescape(row[indexer.Find("Description")]),
            };
            if (string.IsNullOrEmpty(ending.Condition))
                ending.Condition = "true";
            phonecalls.Add(ending);
        }
        string path = Path.Combine(Application.dataPath, ENDING_DATA_PATH);
        JsonUtils.WriteFile(path, phonecalls);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    private void _LoadPeople(string[][] table)
    {
        var indexer = new Indexer(table.First());
        List<Person> people = new List<Person>();
        foreach (string[] row in table.Skip(1))
        {
            var person = new Person
            {
                Name = row[indexer.Find("Person")],
                AvatarId = row[indexer.Find("Avatar")],
                Frame = int.Parse(row[indexer.Find("Frame")]),
            };
            people.Add(person);
        }
        string path = Path.Combine(Application.dataPath, PEOPLE_DATA_PATH);
        JsonUtils.WriteFile(path, people);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private string _Unescape(string s) {
        if (s.Length<2 || s.First() != '"' || s.Last() != '"')
            return s;
        return s.Substring(1, s.Length - 2)
            .Replace("\\\"", "\"")
            .Replace("\"\"", "\"")
            .Replace("\\n", "\n")
            .Replace("\\r", "\r");
    }

    private string _RemoveLineWrap(string s)
    {
        return s.Replace(' ', '\u00A0');
    }
    #endregion
}
#endif
