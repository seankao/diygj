﻿using System.IO;
using Newtonsoft.Json;

public class JsonUtils
{
    public static void WriteFile(string path, object data)
    {
        using (var writer = new StreamWriter(path))
        {
            writer.Write(JsonConvert.SerializeObject(data));
            writer.Flush();
            writer.Close();
        }
    }
}