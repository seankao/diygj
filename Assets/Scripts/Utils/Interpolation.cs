﻿using System;
using UnityEngine;

namespace Operator
{
    using Interpolator = Func<float, float>;

    public static class Interpolation
    {
        public static float Linear(float t)
            => t;
        
        public static Interpolator Accel(float a)
            => t => Accel(t, a);

        public static float Accel(float t, float a)
            => Mathf.Pow(t, a * 2);

        public static Interpolator Decel(float a)
            => t => Decel(t, a);

        public static float Decel(float t, float a)
            => 1 - Mathf.Pow(1 - t, a * 2);
    }
}