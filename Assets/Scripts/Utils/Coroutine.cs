﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Operator
{
    public class Coroutine
    {
        Stack<IEnumerator> stack;
        public bool IsFinished { get; private set; } = false;
        public IEnumerator curr;

        public Coroutine(IEnumerator coroutine)
        {
            IsFinished = false;
            stack = new Stack<IEnumerator>();
            stack.Push(coroutine);
        }

        public void Update()
        {
            while (!IsFinished)
            {
                bool hasNext;
                if (curr == null)
                {
                    if (stack.Count > 0)
                    {
                        curr = stack.Pop();
                    }
                    else
                    {
                        IsFinished = true;
                        continue;
                    }
                }


                hasNext = curr.MoveNext();
                if (!hasNext)
                {
                    curr = null;
                    continue;
                }
                var child = curr.Current;
                if (child is IEnumerator subroutine)
                {
                    stack.Push(curr);
                    curr = subroutine;
                    continue;
                }
                else
                {
                    return;
                }
            }
        }

        public IEnumerator Run()
        {
            while (!IsFinished)
            {
                Update();
                yield return null;
            }
        }
    }
}