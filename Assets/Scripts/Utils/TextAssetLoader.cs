﻿using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;

public static class TextAssetLoader
{
    public static async UniTask<T> LoadAsync<T>(string path, CancellationToken ct)
    {
        var textAsset = await Resources.LoadAsync<TextAsset>(path).ToUniTask(cancellationToken: ct) as TextAsset;
        return JsonConvert.DeserializeObject<T>(textAsset.text);
    }

    public static T Load<T>(string path)
    {
        var textAsset = Resources.Load<TextAsset>(path) as TextAsset;
        return JsonConvert.DeserializeObject<T>(textAsset.text);
    }
}