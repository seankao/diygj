﻿using UnityEngine;
using UnityEngine.UI;

public class EmptyGraphic : Graphic
{
    protected override void OnPopulateMesh(VertexHelper vh)
    {
        base.OnPopulateMesh(vh);
        vh.Clear();
    }
}
