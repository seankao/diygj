﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Operator.Ending
{
    public class EndingIndicator : MonoBehaviour
    {
        [System.Serializable]
        public class EntryData
        {
            public string EndingId;
            public EndingIndicatorEntry.State State;
        }

        [System.Serializable]
        public class EntryView
        {
            public string EndingId;
            public EndingIndicatorEntry View;
        }


        [SerializeField]
        private CanvasGroup _panel;
        [SerializeField]
        private EntryView[] _entries;

        private Dictionary<string, EndingIndicatorEntry> _entryMap;

        private void Awake()
        {
            _entryMap = _entries.ToDictionary(entry => entry.EndingId, entry => entry.View);
        }

        public void SetData(EntryData[] data)
        {
            foreach (string id in _entryMap.Keys)
                _entryMap[id].SetState(EndingIndicatorEntry.State.Incompleted);
            foreach (var d in data)
                SetData(d);
        }

        public void SetData(EntryData data)
        {
            if (!_entryMap.ContainsKey(data.EndingId))
                return;
            _entryMap[data.EndingId].SetState(data.State);
        }

        public IEnumerator Enter(float duration)
        {
            return _panel.FadeIn(duration);
        }
    }

}