﻿using System.Threading;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

namespace Operator.Ending
{
    using Data;
    using System;

    public class EndingScene : MonoBehaviour
    {
        [SerializeField]
        private Image _cg;
        [SerializeField]
        private Text _descriptionView;

        [SerializeField]
        private CanvasGroup _endingPanel;

        [SerializeField]
        private Text _titleView;
        [SerializeField]
        private EndingIndicator _endingIndicator;
        [SerializeField]
        private Button _backToMenuButton;

        private Ending[] _endings;
        private CancellationTokenSource _skip = new CancellationTokenSource();
        private UniTaskCompletionSource _input = new UniTaskCompletionSource();

        private void Awake()
        {
            _backToMenuButton.onClick.AddListener(() => _input.TrySetResult());
        }

        private void Update()
        {
            if(Input.GetMouseButton(0))
                _skip.Cancel();
        }

        public async UniTask Run(string endingId, CancellationToken ct)
        {
            await _Init(endingId, ct);
            await _Enter(endingId, ct);
        }

        private async UniTask _Init(string endingId, CancellationToken ct)
        {
            Color c = new Color(1f, 1f, 1f, 0f);
            _cg.color = c;
            _cg.sprite = Resources.Load<Sprite>($"Ending/{endingId}");
            _descriptionView.text = string.Empty;
        }

        private async UniTask _DisplayDescription(string description, CancellationToken ct)
        {
            try
            {
                await Easing.Play(0, description.Length, description.Length * 0.04f, Easing.Interpolators.Linear, fLength =>
                    {
                        _descriptionView.text = description.Substring(0, Mathf.CeilToInt(fLength));
                    })
                    .ToUniTask(cancellationToken: ct);
            }
            catch(OperationCanceledException e)
            { }
            finally
            {
                _descriptionView.text = description;
            }
        }

        private async UniTask _Enter(string endingId, CancellationToken ct)
        {
            _endings = await TextAssetLoader.LoadAsync<Ending[]>("DataSheets/endings", ct);

            PlayerRecord playerRecord = PlayerRecord.Load();

            var currentEnding = _endings.First(ending => ending.Id == endingId);
            var endingStates = _endings
                .Select(ending => new EndingIndicator.EntryData
                {
                    EndingId = ending.Id,
                    State = ending.Id == endingId
                        ? EndingIndicatorEntry.State.Current
                        : playerRecord.Endings.Contains(ending.Id)
                            ? EndingIndicatorEntry.State.Completed
                            : EndingIndicatorEntry.State.Incompleted
                })
                .ToArray();
            if (!playerRecord.Endings.Contains(endingId))
                playerRecord.Endings = playerRecord.Endings.Append(endingId).ToArray();
            PlayerRecord.Save(playerRecord);

            _titleView.text = currentEnding.Title;
            _endingIndicator.SetData(endingStates);

            await _cg.FadeIn(1f).ToUniTask();

            await _DisplayDescription(currentEnding.Description, (_skip = new CancellationTokenSource()).Token);

            await UniTask.Delay(System.TimeSpan.FromSeconds(0.8f));

            await _endingPanel.FadeIn(1f).ToUniTask();
            _endingPanel.blocksRaycasts = true;

            await _WaitForEnd();
        }

        private async UniTask _WaitForEnd()
        {
            _input = new UniTaskCompletionSource();
            await _input.Task;
        }

    }
}

