﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Operator.Ending
{
    public class EndingIndicatorEntry : MonoBehaviour
    {
        public enum State
        {
            Incompleted,
            Completed,
            Current,
        }

        [System.Serializable]
        private class Skin
        {
            public Color Incompleted;
            public Color Completed;
            public Color Current;
        }

        [SerializeField]
        private Skin _skin;
        [SerializeField]
        private Image _view;

        public void SetState(State state)
        {
            _view.color = _GetColor(state);
        }

        private Color _GetColor(State state)
        {
            switch (state)
            {
                case State.Current:
                    return _skin.Current;
                case State.Completed:
                    return _skin.Completed;
                case State.Incompleted:
                default:
                    return _skin.Incompleted;
            }
        }
    }
}