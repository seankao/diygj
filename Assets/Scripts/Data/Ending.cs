﻿using UnityEngine;
using UnityEditor;

namespace Operator.Data
{
    public class Ending
    {
        public string Id;
        public string Condition;
        public string Title;
        public string Description;
    }
}
