﻿using UnityEngine;
using UnityEditor;

public class Option
{
    public string Id;
    public string PhoneCallId;
    public string Receiver;
    public string Eval;
    public string Condition;
    public string Result;
}