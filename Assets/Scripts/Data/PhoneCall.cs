﻿using UnityEngine;
using UnityEditor;

public class PhoneCall
{
    public string Id;
    public string Caller;
    public string AvatarId;
    public string Content;
    public string Condition;
    public string Info;
    public int Frame;
}