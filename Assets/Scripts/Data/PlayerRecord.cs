﻿using System;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using System.IO;

namespace Operator.Data
{
    public class PlayerRecord
    {
        public string[] Endings;

        public static PlayerRecord Load()
        {
            string path = Path.Combine(Application.persistentDataPath, "global.dat");
            if (!File.Exists(path))
                return new PlayerRecord();
            return JsonConvert.DeserializeObject<PlayerRecord>(File.ReadAllText(path));
        }

        public static void Save(PlayerRecord playerRecord)
        {
            string path = Path.Combine(Application.persistentDataPath, "global.dat");
            JsonUtils.WriteFile(path, playerRecord);
        }

        public PlayerRecord()
        {
            Endings = new string[0];
        }
    }
}
