﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Receiver : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
{
    //TODO : 需要某種struct/class來存接收者的資料（頭像之類的）

    public string testUseName;
    public Text nameView;
    public Image avatarView;
    public Person Data { get; private set; }

    private PhoneCallManager manager;
    public RectTransform callerLineAttachPoint;

    public Image port;
    public Sprite portNormal;
    public Sprite portPressed;

    private void Start()
    {
        manager = PhoneCallManager.instance;
    }

    public void SetData(Person data)
    {
        Data = data;
        nameView.text = data.Name;
        avatarView.sprite = CallerFactory.GetAvatar(data.AvatarId);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        manager.RequestConnection(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        manager.SetHoveringReceiver(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        manager.SetHoveringReceiver(null);
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        port.sprite = portNormal;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        port.sprite = portPressed;
    }
}
