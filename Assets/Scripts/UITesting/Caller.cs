﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

using Operator.Data;

public class Caller : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
{
    //TODO : 需要某種struct/class來存來電者的資料（頭像、詳細通訊內容之類的）

    public string testUseName;
    public Text nameView;
    public Text contentView;
    public Image avatarView;

    public PhoneCall Data { get; private set; }
    private bool isPending = false;
    private PhoneCallManager manager;

    public UILineRenderer lr;
    public RectTransform lrRect;
    public Image port;
    public Sprite portNormal;
    public Sprite portPressed;

    private Canvas canvas;

    public event Action<PhoneCall> OnHoverOn;
    public event Action<PhoneCall> OnHoverOff;

    private void Start()
    {
        manager = PhoneCallManager.instance;
        manager.event_PendingCallerChange.AddListener(caller=>
        {
            isPending = (caller == this);

            if (isPending)
            {
                lrRect.SetParent(canvas.transform);
                lrRect.SetAsFirstSibling();
            }    
            else
            {
                lr.SetPoints(new Vector2[] { });
                lrRect.SetParent(transform);
            }    
        });

        canvas = GetComponentInParent<Canvas>();
    }

    private void Update()
    {
        if (isPending)
            SetLine();
    }

    private void SetLine()
    {
        Vector2 start = Vector2.zero;
        Vector2 endRaw = (manager.hoveringReceiver == null)? (Vector2)Input.mousePosition : TransformUtility.instance.UGUITransformToScreenPoint(manager.hoveringReceiver.callerLineAttachPoint);
        Vector2 end = (endRaw - TransformUtility.instance.UGUITransformToScreenPoint(lrRect)) / canvas.scaleFactor;

        Vector2[] points = new Vector2[31];
        points[0] = start;
        points[30] = end;

        for (int i = 1; i <= 29; i++)
        {
            float progress = i / 30f;
            points[i] = start + new Vector2(end.x * EaseLibrary.CallEaseFunction(Easing.InOutCubic, progress), end.y * progress);
        }

        lr.SetPoints(points);
    }

    public void SetData(PhoneCall data)
    {
        Data = data;
        nameView.text = data.Caller;
        contentView.text = data.Content;
        avatarView.sprite = CallerFactory.GetAvatar(data.AvatarId);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        manager.SetPendingCaller(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        OnHoverOn?.Invoke(Data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnHoverOff?.Invoke(Data);
    }

    private void OnDestroy()
    {
        if(lr != null)
            Destroy(lr.gameObject);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        port.sprite = portNormal;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        port.sprite = portPressed;
    }
}
