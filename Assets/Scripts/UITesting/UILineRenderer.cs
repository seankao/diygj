﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class UILineRenderer : Graphic
{
    [Space(20), SerializeField]
    public bool useWorldSpace;
    [SerializeField]
    public Gradient gradient = new Gradient();
    [SerializeField]
    public float lineWidth;
    [SerializeField]
    private List<Vector2> points = new List<Vector2>();
    private int pointCount { get { return points.Count; } }
    
    private List<Vector2> tempVertsHolder = new List<Vector2>();

    private RectTransform rect = null;

    private void Update()
    {
        SetVerticesDirty();
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        if (rect == null)
            rect = GetComponent<RectTransform>();

        vh.Clear();

        if (points.Count < 2)
            return;

        tempVertsHolder.Clear();
        UIVertex vertex = UIVertex.simpleVert;
        float alpha = (2 - Mathf.Sqrt(4 - 4 * color.a)) / 2;
        vertex.color = new Color(color.r, color.g, color.b, alpha);

        float totalLen = 0;
        float currentLen = 0;
        for (int i = 1; i < points.Count; i++)
            totalLen += (points[i] - points[i - 1]).magnitude;

        //避免出現除以零的情況
        if (totalLen == 0)
            totalLen = 1;

        for (int i = 0; i < points.Count; i++)
        {
            if(i == 0)
            {
                Vector2 direction = (points[1] - points[0]).normalized;
                Vector2 normal = Vector2.Perpendicular(direction).normalized;

                tempVertsHolder.Add(points[0] + (normal * lineWidth / 2));
                tempVertsHolder.Add(points[0] - (normal * lineWidth / 2));
            }
            else if(i == points.Count - 1)
            {
                Vector2 direction = (points[i] - points[i - 1]).normalized;
                Vector2 normal = Vector2.Perpendicular(direction).normalized;

                tempVertsHolder.Add(points[i] + (normal * lineWidth / 2));
                tempVertsHolder.Add(points[i] - (normal * lineWidth / 2));
            }
            else
            {
                Vector2 directionA = (points[i] - points[i - 1]).normalized;
                Vector2 directionB = (points[i] - points[i + 1]).normalized;
                Vector2 bisector = (directionA + directionB).normalized;

                if(bisector.magnitude == 0)
                    bisector = Vector2.Perpendicular(directionA).normalized;

                float projectionLen = Vector3.Project(bisector, Vector2.Perpendicular(directionA)).magnitude;

                tempVertsHolder.Add(points[i] + (bisector * ((lineWidth / 2) / projectionLen)));
                tempVertsHolder.Add(points[i] - (bisector * ((lineWidth / 2) / projectionLen)));
            }
        }

        if(useWorldSpace)
        {
            float cosine = Mathf.Cos(rect.localRotation.eulerAngles.z * Mathf.Deg2Rad);
            float sine = Mathf.Sin(rect.localRotation.eulerAngles.z * Mathf.Deg2Rad);

            for (int i = 0; i < tempVertsHolder.Count; i++)
            {
                tempVertsHolder[i] = new Vector2(tempVertsHolder[i].x - rect.localPosition.x, tempVertsHolder[i].y - rect.localPosition.y);
                tempVertsHolder[i] = new Vector2(cosine * tempVertsHolder[i].x + sine * tempVertsHolder[i].y, -sine * tempVertsHolder[i].x + cosine * tempVertsHolder[i].y);
                tempVertsHolder[i] = new Vector2(tempVertsHolder[i].x / rect.localScale.x, tempVertsHolder[i].y / rect.localScale.y);
            }
        }

        for (int i = 0; i < points.Count; i++)
        {
            vertex.color = gradient.Evaluate(currentLen / totalLen) * color;
            vertex.position = tempVertsHolder[2 * i];
            vh.AddVert(vertex);
            vertex.position = tempVertsHolder[2 * i + 1];
            vh.AddVert(vertex);

            if (i != points.Count - 1)
                currentLen += (points[i + 1] - points[i]).magnitude;
        }

        for (int i = 0; i < points.Count - 1; i++)
        {
            //對，沒錯，這邊會畫兩次

            vh.AddTriangle(2 * i, 2 * i + 1, 2 * i + 2);
            vh.AddTriangle(2 * i, 2 * i + 1, 2 * i + 3);
            vh.AddTriangle(2 * i, 2 * i + 2, 2 * i + 3);
            vh.AddTriangle(2 * i + 1, 2 * i + 2, 2 * i + 3);
        }
    }

    public void SetPoints(Vector2[] vectors)
    {
        points.Clear();

        for (int i = 0; i < vectors.Length; i++)
            points.Add(vectors[i]);
    }

    public void SetPoint(int index, Vector2 point)
    {
        if(index >= points.Count || index < 0)
        {
            Debug.LogError("UILineRenderer SetPoint Error : Index out of bound.");
            return;
        }
        points[index] = point;
    }

    public Vector2 GetPoint(int index)
    {
        if (index >= points.Count || index < 0)
        {
            Debug.LogError("UILineRenderer GetPoint Error : Index out of bound.");
            return Vector2.zero;
        }
        return points[index];
    }
}
