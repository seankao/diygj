﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneCallManager : MonoBehaviour
{
    public static PhoneCallManager instance { get; private set; } = null;
    
    private List<Caller> callers = new List<Caller>();
    private List<Receiver> receivers = new List<Receiver>();
    private Caller pendingCaller = null;
    public Receiver hoveringReceiver { get; private set; } = null;

    [SerializeField]
    private GameObject[] callerPrefabs;        //這東西應該要有Caller掛在上面
    [SerializeField]
    private GameObject[] receiverPrefabs;      //這東西應該要有Receiver掛在上面
    [SerializeField]
    private Transform callersGridGroup;
    [SerializeField]
    private Transform receiversGridGroup;

    [SerializeField]
    private AudioSource callConnectAudio;
    
    public event Action<PhoneCall> OnChangeHoveringCaller;

    public ObjectEvent<Caller> event_AddCaller { get; private set; } = new ObjectEvent<Caller>();
    public ObjectEvent<Caller> event_RemoveCaller { get; private set; } = new ObjectEvent<Caller>();
    public ObjectEvent<Caller> event_PendingCallerChange { get; private set; } = new ObjectEvent<Caller>();
    public ObjectEvent<PhoneCall, Person> event_Connect { get; private set; } = new ObjectEvent<PhoneCall, Person>();

    private void Awake()
    {
        if(instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void AddCaller(PhoneCall data)
    {
        Caller caller = Instantiate(callerPrefabs[data.Frame-1], callersGridGroup).GetComponent<Caller>();
        caller.SetData(data);
        caller.OnHoverOn += _SetHovering;
        caller.OnHoverOff += _ => _CancelHover();
        callers.Add(caller);
        event_AddCaller.Invoke(caller);
    }

    private void _SetHovering(PhoneCall data)
    {
        OnChangeHoveringCaller?.Invoke(data);
    }

    private void _CancelHover()
    {
        OnChangeHoveringCaller?.Invoke(null);
    }

    public void RemoveCaller(string phonecallId)
    {
        var caller = callers.Find(c => c.Data.Id == phonecallId);
        if (caller != null)
            _RemoveCaller(caller);
    }

    private void _RemoveCaller(Caller caller)
    {
        callers.Remove(caller);
        event_RemoveCaller.Invoke(caller);
        Destroy(caller.gameObject);
    }

    public void AddReceiver(Person data)
    {
        Receiver receiver = Instantiate(receiverPrefabs[data.Frame-1], receiversGridGroup).GetComponent<Receiver>();
        receiver.SetData(data);
        receivers.Add(receiver);
    }

    public void SetPendingCaller(Caller caller)
    {
        if (pendingCaller == caller)
            pendingCaller = null;
        else
            pendingCaller = caller;
        event_PendingCallerChange.Invoke(pendingCaller);
    }

    public void SetHoveringReceiver(Receiver receiver)
    {
        hoveringReceiver = receiver;
    }

    public void RequestConnection(Receiver receiver)
    {
        if(pendingCaller != null)
        {
            callConnectAudio.Play();
            event_Connect.Invoke(pendingCaller.Data, receiver.Data);
            pendingCaller = null;
            event_PendingCallerChange.Invoke(pendingCaller);
            _CancelHover();
        }
    }
}
