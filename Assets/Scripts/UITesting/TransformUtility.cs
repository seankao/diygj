﻿using UnityEngine;

public class TransformUtility : MonoBehaviour
{
    /// <summary>
    /// 遊戲主鏡頭
    /// </summary>
    [SerializeField]
    public Camera ScenceCamera;

    /// <summary>
    /// UGUI相機
    /// </summary>
    [SerializeField]
    public Camera UGUICamera;

    public static TransformUtility instance { get; private set; } = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    /// <summary>
    /// 世界座標 -> 螢幕座標
    /// </summary>
    public Vector2 WorldToScreenPoint(Vector3 worldPoint)
    {
        return RectTransformUtility.WorldToScreenPoint(ScenceCamera, worldPoint);
    }

    ///<summary>
    ///螢幕座標 -> 世界座標
    ///</summary>
    public bool ScreenPointToWorldPointInRectangle(RectTransform rect, Vector2 screenPoint, out Vector3 worldPoint)
    {
        return RectTransformUtility.ScreenPointToWorldPointInRectangle(rect, screenPoint, UGUICamera, out worldPoint);
    }

    ///<summary>
    ///螢幕座標 -> UGUI內某個RectTransform下的localPosition座標
    ///</summary>
    public bool ScreenPointToUGUILocalPointInRectangle(RectTransform rect, Vector2 screenPoint, out Vector2 localPoint)
    {
        return RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screenPoint, UGUICamera, out localPoint);
    }

    ///<summary>
    ///UGUI內某個RectTransform -> 螢幕座標
    ///</summary>
    public Vector2 UGUITransformToScreenPoint(RectTransform rect)
    {
        return RectTransformUtility.WorldToScreenPoint(ScenceCamera, rect.transform.position);
    }

    ///<summary>
    ///UGUI內某個RectTransform -> 世界座標（這應該不常用到，你需要的比較有可能是UGUITransformToScreenPoint()）
    ///</summary>
    public Vector2 UGUITransformToWorldPoint(RectTransform rect)
    {
        return rect.transform.position;
    }
}
