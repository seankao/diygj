﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntEvent : UnityEvent<int> { }
public class FloatEvent : UnityEvent<float> { }
public class BoolEvent : UnityEvent<bool> { }
public class GameObjectEvent : UnityEvent<GameObject> { }
public class ObjectEvent<T> : UnityEvent<T> { }
public class ObjectEvent<T1, T2> : UnityEvent<T1, T2> { }
