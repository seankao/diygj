﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using NLua;

namespace Operator
{
    using Tutorial;
    using Menu;
    using Gameplay;
    using Ending;

    public class Main : MonoBehaviour
    {
        private CancellationTokenSource _cts = new CancellationTokenSource();

        private async void Awake()
        {
            DontDestroyOnLoad(gameObject);
            while (true)
            {
                await _Menu(_cts.Token);
                await _Tutorial(_cts.Token);
                var gameplayResult = await _Gameplay(_cts.Token);
                await _Ending(gameplayResult.EndingId, _cts.Token);
            }
        }
        
        private void OnDestroy()
        {
            _cts.Cancel();
        }

        private async UniTask _Menu(CancellationToken ct)
        {
            await _LoadScene("Menu", ct);
            var scene = FindObjectOfType<MenuScene>();
            await scene.Run(ct);
        }

        private async UniTask _Tutorial(CancellationToken ct)
        {
            await _LoadScene("Tutorial", ct);
            var scene = FindObjectOfType<TutorialScene>();
            await scene.Run(ct);
        }

        private async UniTask<GameplayResult> _Gameplay(CancellationToken ct)
        {
            await _LoadScene("Gameplay", ct);
            var scene = FindObjectOfType<GameplayScene>();
            return await scene.Run(ct);
        }

        private async UniTask _Ending(string endingId, CancellationToken ct)
        {
            await _LoadScene("Ending", ct);
            var scene = FindObjectOfType<EndingScene>();
            await scene.Run(endingId, ct);
        }

        static async UniTask _LoadScene(string sceneName, CancellationToken ct)
        {
            await _WaitSceneLoading(ct, SceneManager.LoadSceneAsync(sceneName));
        }
        
        private static async UniTask _WaitSceneLoading(CancellationToken ct, AsyncOperation op)
        {
            op.allowSceneActivation = false;
            await UniTask.WaitUntil(() => op.progress >= 0.9f, cancellationToken: ct);
            op.allowSceneActivation = true;
            await UniTask.WaitUntil(() => op.isDone, cancellationToken: ct);
        }
    }
}

